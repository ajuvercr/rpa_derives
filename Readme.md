<div align="center">
    <img src="logo.png" height="200">
</div>

<a href="https://crates.io/crates/rpa_derives">![RPA Derives Version](https://img.shields.io/badge/crates.io-v0.4.0-orange.svg?longCache=true)</a>

### RPA Derives
This library is a module of the Rust Persistence API.
Please visit [rpa](https://gitlab.com/gexuy/public-libraries/rust/rpa) for more information.