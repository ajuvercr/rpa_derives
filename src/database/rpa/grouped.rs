use proc_macro2::{Span, TokenStream};
use quote::quote;
/**
    Rpa (Rust Persistence API) Derive Belongs Partial implementation.
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use syn::Ident;

pub fn get_impl(
    type_name: &syn::Ident,
    connection_impl: &TokenStream,
    belongs: &Vec<String>,
) -> Vec<proc_macro2::TokenStream> {
    let mut grouped_impl: Vec<proc_macro2::TokenStream> = Vec::new();

    for belong_string in belongs {
        let method_name = Ident::new(
            (String::from("find_grouped_by_") + &belong_string.to_lowercase()).as_str(),
            Span::call_site(),
        );
        let belong: Ident = Ident::new(belong_string.as_str(), Span::call_site());
        grouped_impl.push(quote! {
            pub fn #method_name(parents: &Vec<#belong>, connection: &#connection_impl) -> ::std::result::Result<Vec<(#belong, Vec<#type_name>)>, ::rpa::RpaError> {
                let find_result = #type_name::belonging_to(parents)
                    .load::<#type_name>(connection);
                let error_message = format!("A problem has occurred finding {} grouped by {}", stringify!(#type_name), stringify!(#belong));
                if find_result.is_err() {
                    let result_error = find_result.err().unwrap();
                    return ::rpa::RpaError::build_from::<Vec<(#belong, Vec<#type_name>)>>(error_message, result_error);
                }
                let grouped_by = find_result.unwrap().grouped_by(parents);
                let zipped = parents.clone().into_iter().zip(grouped_by).collect::<Vec<_>>();
                Ok(zipped)
            }
        });
    }

    grouped_impl
}
