use proc_macro2::TokenStream;
/**
    Rpa (Rust Persistence API) Derive Find Partial implementation.
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use quote::quote;

pub fn get_impl(
    type_name: &syn::Ident,
    connection_impl: &TokenStream,
    table_name: &syn::Ident,
) -> proc_macro2::TokenStream {
    quote! {
        fn search(search_request: ::rpa::SearchRequest, connection: &#connection_impl) -> ::std::result::Result<::rpa::SearchResponse<#type_name>, ::rpa::RpaError> {
            ::rpa::do_search::<#type_name>(search_request, stringify!(#table_name).to_string(), connection)
        }
    }
}
