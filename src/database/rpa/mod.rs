use crate::generic::connection_from_string;
use proc_macro::TokenStream;
use proc_macro2::Span;
use quote::quote;
/**
    Rpa (Rust Persistence API) Derive implementation.
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use std::collections::HashMap;
use syn::Ident;

pub mod belongs;
pub mod delete;
pub mod find;
pub mod grouped;
pub mod json;
pub mod save;
pub mod search;
pub mod update;

pub fn impl_rpa(
    type_name: &syn::Ident,
    _fields: &syn::Fields,
    attributes: HashMap<String, Vec<String>>,
) -> TokenStream {
    let table_name_string: &String = attributes.get("table_name").unwrap().first().unwrap();
    let table_name: Ident = Ident::new(table_name_string.as_str(), Span::call_site());

    let mut belongs: &Vec<String> = &Vec::new();

    if attributes.get("belongs_to").is_some() {
        belongs = attributes.get("belongs_to").unwrap();
    }

    // Default connection
    let mut connection_type: &Vec<String> = &vec![String::from("MYSQL")];

    if attributes.get("connection_type").is_some() {
        connection_type = attributes.get("connection_type").unwrap();
    }

    let connection_impl = connection_from_string(connection_type.first().unwrap());
    let json_impl = json::get_impl(&type_name);
    let save_impl = save::get_impl(&type_name, &connection_impl, &table_name);
    let find_impl = find::get_impl(&type_name, &connection_impl, &table_name);
    let update_impl = update::get_impl(&type_name, &connection_impl, &table_name);
    let delete_impl = delete::get_impl(&type_name, &connection_impl, &table_name);
    let belong_impl = belongs::get_impl(&type_name, &connection_impl, &belongs);
    let grouped_impl = grouped::get_impl(&type_name, &connection_impl, &belongs);
    let search_impl = search::get_impl(&type_name, &connection_impl, &table_name);

    let trait_impl = quote! {
        impl ::rpa::Rpa<#connection_impl> for #type_name {
            #json_impl
            #save_impl
            #find_impl
            #update_impl
            #delete_impl
            #search_impl
        }
        impl #type_name {
            #(#belong_impl)*
            #(#grouped_impl)*
        }
    };

    let code_generated = quote! {
        #trait_impl
    };

    code_generated.into()
}
