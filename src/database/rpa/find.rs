use proc_macro2::TokenStream;
/**
    Rpa (Rust Persistence API) Derive Find Partial implementation.
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use quote::quote;

pub fn get_impl(
    type_name: &syn::Ident,
    connection_impl: &TokenStream,
    table_name: &syn::Ident,
) -> proc_macro2::TokenStream {
    quote! {
        fn find(entity_id: &String, connection: &#connection_impl) -> ::std::result::Result<#type_name, ::rpa::RpaError> {
            use ::diesel::QueryDsl;
            use ::diesel::RunQueryDsl;

            let error_message = format!("No {} was found with id {}", stringify!(#type_name), entity_id);
            let result = #table_name::table.find(entity_id).get_result(connection);
            ::rpa::RpaError::map_result::<#type_name>(error_message, result)
        }

        fn exists(entity_id: &String, connection: &#connection_impl) -> ::std::result::Result<bool, ::rpa::RpaError> {
            use ::diesel::QueryDsl;
            use ::diesel::RunQueryDsl;

            let error_message = format!("An error has occurred trying to find a {} with id {}", stringify!(#type_name), entity_id);
            let result: ::std::result::Result<#type_name, ::diesel::result::Error> = #table_name::table.find(entity_id).first(connection);
            if result.is_err() {
                let result_error = result.err().unwrap();
                if result_error == ::diesel::result::Error::NotFound {
                    return Ok(false);
                } else {
                    return ::rpa::RpaError::build_from::<bool>(error_message, result_error);
                }
            }
            Ok(true)
        }

        fn find_all(connection: &#connection_impl) -> ::std::result::Result<Vec<#type_name>, ::rpa::RpaError> {
            use ::diesel::QueryDsl;
            use ::diesel::RunQueryDsl;

            let error_message = format!("No {} was found", stringify!(#type_name));
            let result = #table_name::table.order(#table_name::id).load::<#type_name>(connection);
            ::rpa::RpaError::map_result::<Vec<#type_name>>(error_message, result)
        }
    }
}
