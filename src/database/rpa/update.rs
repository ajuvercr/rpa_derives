use proc_macro2::TokenStream;
/**
    Rpa (Rust Persistence API) Derive Update Partial implementation.
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use quote::quote;

pub fn get_impl(
    type_name: &syn::Ident,
    connection_impl: &TokenStream,
    table_name: &syn::Ident,
) -> proc_macro2::TokenStream {
    quote! {
        fn update(entity_id: &String, entity: &#type_name, connection: &#connection_impl) -> ::std::result::Result<usize, ::rpa::RpaError> {
            use ::diesel::RunQueryDsl;
            use ::diesel::QueryDsl;

            let error_message = format!("A problem has occurred updating {} with id {}", stringify!(#type_name), entity_id);
            let result = ::diesel::update(#table_name::table.find(entity_id))
                                .set(entity)
                                .execute(connection);
            ::rpa::RpaError::map_result::<usize>(error_message, result)
        }
        fn update_self(self: Self, connection: &#connection_impl) -> ::std::result::Result<usize, ::rpa::RpaError> {
            use ::diesel::RunQueryDsl;
            use ::diesel::QueryDsl;

            let error_message = format!("A problem has occurred updating {} with id {}", stringify!(#type_name), &self.id);
            let result = ::diesel::update(#table_name::table.find(&self.id)).set(&self).execute(connection);
            ::rpa::RpaError::map_result::<usize>(error_message, result)
        }
    }
}
