/**
    Rpa (Rust Persistence API) Derive definition.
    This function derives any custom macro and gets the attributes and tokens to work with.
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
use crate::proc_macro::TokenStream;
use quote::quote;
use std::collections::HashMap;
use syn::{Lit, Meta, MetaList, MetaNameValue};

// Generic derive macro fn
pub fn derive_macro<F>(tokens: TokenStream, gen_impl: F) -> TokenStream
where
    F: Fn(&syn::Ident, &syn::Fields, HashMap<String, Vec<String>>) -> TokenStream,
{
    // Parse TokenStream into AST
    let ast: syn::ItemStruct = syn::parse(tokens).unwrap();
    let mut attributes: HashMap<String, Vec<String>> = HashMap::new();
    // Iterate over the struct's #[...] attributes
    for attr in ast.attrs.into_iter() {
        let option = attr.parse_meta().unwrap();
        match option {
            Meta::List(MetaList {
                ref path,
                ref nested,
                ..
            }) => {
                let segment = path.segments.first().unwrap();
                let ident = segment.ident.clone();
                let mut values: Vec<String> = Vec::new();
                if attributes.get(ident.to_string().as_str()).is_some() {
                    values.extend(
                        attributes
                            .get(ident.to_string().as_str())
                            .unwrap()
                            .iter()
                            .cloned(),
                    );
                }
                for nested_meta in nested.iter() {
                    match nested_meta {
                        syn::NestedMeta::Meta(meta) => match meta {
                            Meta::Path(path) => {
                                let segment = path.segments.first().unwrap();
                                let ident = segment.ident.clone();
                                values.push(ident.to_string());
                                ()
                            }
                            _ => (),
                        },
                        _ => (),
                    }
                }
                attributes.insert(ident.to_string(), values);
                ()
            }
            Meta::NameValue(MetaNameValue {
                ref path, ref lit, ..
            }) => {
                let segment = path.segments.first().unwrap();
                let ident = segment.ident.clone();
                if let Lit::Str(lit) = lit {
                    let mut values: Vec<String> = Vec::new();
                    if attributes.get(ident.to_string().as_str()).is_some() {
                        values.extend(
                            attributes
                                .get(ident.to_string().as_str())
                                .unwrap()
                                .iter()
                                .cloned(),
                        );
                    }
                    values.push(lit.value());
                    attributes.insert(ident.to_string(), values);
                }
            }
            _ => (),
        }
    }

    // Build the trait implementation
    gen_impl(&ast.ident, &ast.fields, attributes)
}

pub fn connection_from_string(string_name: &str) -> proc_macro2::TokenStream {
    match string_name {
        "MYSQL" => quote! {
            ::diesel::mysql::MysqlConnection
        },

        "POSTGRESQL" => quote! {
            ::diesel::pg::PgConnection
        },

        "SQLITE" => quote! {
            ::diesel::sqlite::SqliteConnection
        },

        _ => quote! {
            ::diesel::mysql::MysqlConnection
        },
    }
}
